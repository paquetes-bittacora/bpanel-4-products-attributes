<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductAttributes;

use Bittacora\Bpanel4\ProductAttributes\Commands\InstallCommand;
use Illuminate\Support\ServiceProvider;

final class Bpanel4ProductAttributesServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->commands([InstallCommand::class]);
    }
}