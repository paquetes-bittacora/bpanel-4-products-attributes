<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductAttributes\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Bittacora\Tabs\Tabs;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    public $signature = 'bpanel4-product-attributes:install';
    public $description = 'Instala el módulo de atributos para los productos';

    private const PERMISSIONS = [
        'attributes.index',
        'attributes.create',
        'attributes.show',
        'attributes.edit',
        'attributes.destroy',
        'attributes.store',
        'attributes.update',
    ];

    public function handle(AdminMenu $adminMenu): void
    {
        $this->comment('Instalando el módulo Bpanel4 Product Attributes...');

        $this->createMenuEntries($adminMenu);
        $this->createTabs();

        $this->giveAdminPermissions();
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'bpanel4-products.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }

    private function createTabs(): void
    {
        $this->comment('Añadiendo pestañas...');
        Tabs::createItem(
            'bpanel4-products.attributes.index',
            'bpanel4-products.attributes.create',
            'bpanel4-products.attributes.create',
            'Nuevo',
            'fa fa-plus'
        );

        Tabs::createItem(
            'bpanel4-products.attributes.index',
            'bpanel4-products.attributes.index',
            'bpanel4-products.attributes.index',
            'Listar',
            'fa fa-list'
        );

        Tabs::createItem(
            'bpanel4-products.attributes.create',
            'bpanel4-products.attributes.create',
            'bpanel4-products.attributes.create',
            'Nuevo',
            'fa fa-plus'
        );
    }

    private function createMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo al menú de administración...');

        $adminMenu->createAction(
            'bpanel4-products',
            'Atributos',
            'attributes.index',
            'fas fa-tshirt'
        );
    }
}
